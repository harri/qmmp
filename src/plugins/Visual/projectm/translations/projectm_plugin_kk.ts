<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk_KZ">
<context>
    <name>ProjectMPlugin</name>
    <message>
        <location filename="../projectmplugin.cpp" line="41"/>
        <source>ProjectM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectMWidget</name>
    <message>
        <location filename="../projectmwidget.cpp" line="136"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="136"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="137"/>
        <source>&amp;Show Song Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="137"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="138"/>
        <source>&amp;Show Preset Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="138"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="139"/>
        <source>&amp;Show Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="139"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="141"/>
        <source>&amp;Next Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="141"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="142"/>
        <source>&amp;Previous Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="142"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="143"/>
        <source>&amp;Random Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="143"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="144"/>
        <source>&amp;Lock Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="144"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="146"/>
        <source>&amp;Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../projectmwidget.cpp" line="146"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualProjectMFactory</name>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="29"/>
        <source>ProjectM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="49"/>
        <source>About ProjectM Visual Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="50"/>
        <source>Qmmp ProjectM Visual Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="51"/>
        <source>This plugin adds projectM visualization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visualprojectmfactory.cpp" line="53"/>
        <source>Based on libprojectM-qt library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

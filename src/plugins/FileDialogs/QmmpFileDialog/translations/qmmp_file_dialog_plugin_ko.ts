<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>QmmpFileDialog</name>
    <message>
        <location filename="../qmmpfiledialog.ui" line="14"/>
        <source>Add Files</source>
        <translation>파일 추가</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="177"/>
        <source>Up</source>
        <translation>위로</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="180"/>
        <location filename="../qmmpfiledialog.ui" line="193"/>
        <location filename="../qmmpfiledialog.ui" line="212"/>
        <location filename="../qmmpfiledialog.ui" line="237"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="190"/>
        <source>List view</source>
        <translation>목록 보기</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="209"/>
        <source>Detailed view</source>
        <translation>자세히 보기</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="234"/>
        <source>Close dialog on add</source>
        <translation>추가할 때 대화상자 닫기</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="106"/>
        <source>File name:</source>
        <translation>파일 이름:</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="122"/>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="129"/>
        <source>Files of type:</source>
        <translation>파일 유형:</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.ui" line="155"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>QmmpFileDialogFactory</name>
    <message>
        <location filename="../qmmpfiledialog.cpp" line="69"/>
        <location filename="../qmmpfiledialog.cpp" line="79"/>
        <source>Qmmp File Dialog</source>
        <translation>Qmmp 파일 대화상자</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.cpp" line="78"/>
        <source>About Qmmp File Dialog</source>
        <translation>Qmmp 파일 대화상자 정보</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.cpp" line="80"/>
        <source>Written by:
Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;
Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>작성자:
Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;
Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialog.cpp" line="83"/>
        <source>Some code is copied from the Qt library</source>
        <translation>Qt 라이브러리에서 일부 코드가 복사됩니다</translation>
    </message>
</context>
<context>
    <name>QmmpFileDialogImpl</name>
    <message>
        <location filename="../qmmpfiledialogimpl.cpp" line="263"/>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialogimpl.cpp" line="276"/>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialogimpl.cpp" line="290"/>
        <source>Directories</source>
        <translation>디렉토리</translation>
    </message>
    <message>
        <location filename="../qmmpfiledialogimpl.cpp" line="480"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>% 1이(가) 이미 존재합니다.
교체하시겠습니까?</translation>
    </message>
</context>
</TS>
